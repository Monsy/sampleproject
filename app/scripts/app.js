(function() {
'use strict';
/**
 * @ngdoc overview
 * @name companyApp
 * @description
 * # companyApp
 *
 * Main module of the application.
 */
angular
    .module('companyApp', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ui.router',
      'ui.bootstrap',
      'toaster'
    ])
    .config(function ($routeProvider,$stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('site', {
                'abstract': true,
                views:{
                'navbar@':{
                    templateUrl:'scripts/components/navbar/navbar.html',
                    controller:'NavbarController',
                    controllerAs:'vm'
                }
            }
        });
    })

    .run(function($rootScope, $location,authFact,$state) {
        $rootScope.$on('$stateChangeSuccess', function() {
            if(!authFact.getAccessToken()){
                $state.go('login');
            }
        });
    });
})();
