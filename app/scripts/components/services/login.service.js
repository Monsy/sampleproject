(function(){
'use strict';
/**
 * @ngdoc service
 * @name companyApp.LoginService
 * @description all request for login process
 * # LoginService
 * Service in the companyApp.
 */
angular.module('companyApp')
    .service('LoginService', LoginService);

function LoginService($resource) {

    return $resource('scripts/data/users.json', {}, {
    			//method for getting all of the users
               'query':{
                    method:'GET',
                    isArray:true
                }
            });
}

})();
