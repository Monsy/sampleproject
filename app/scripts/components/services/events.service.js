(function(){
'use strict';
/**
 * @ngdoc service
 * @name companyApp.dataService
 * @description
 * # dataService
 * Service in the companyApp.
 */
angular.module('companyApp')
    .service('EventsService', EventsService);

function EventsService($resource) {

    return $resource('scripts/data/events.json', {}, {
        'query':{
            method:'GET',
            isArray:true
        },
        'get':{
            method:'GET',
            url:'api/events/:id',
            transformResponse: function(data){
                return data;
            }
        },
        'updateEvent':{
            method:'POST',
            url:'api/updateevent',
            params:{
                eventObject:{}
            },
            transformResponse: function(data){
                return data;
            }
        },
        'createEvent':{
            method:'POST',
            url:'api/newevent',
            params:{
                eventObject:{}
            },
            transformResponse: function(data){
                return data;
            }
        },
        'deleteEvent':{
            method:'POST',
            url:'api/deleteevent/:eventId',
            transformResponse: function(data){
                return data;
            }
        }
    });

}
})();

