(function(){
'use strict';
/**
 * @ngdoc service
 * @name companyApp.authFact
 * @description set token for the user
 * # authFact
 * Service in the companyApp.
 */
angular.module('companyApp')
    .service('authFact', authFact);

function authFact($cookies) {
    var authFact = {};
    authFact.setAccessToken = function(accessToken){
        $cookies.put('authToken',accessToken);
    }
    authFact.getAccessToken = function(){
        return $cookies.get('authToken');
    }
    authFact.setUserObject = function(userObject){
        $cookies.putObject('userObject', userObject);
    }
    authFact.getUserObject = function(){
        return $cookies.getObject('userObject');
    }
    return authFact;
}

})();

