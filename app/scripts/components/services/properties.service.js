(function(){
'use strict';
/**
 * @ngdoc service
 * @name companyApp.authFact
 * @description
 * # authFact
 * Service in the companyApp.
 */
angular.module('companyApp')
  .service('propertiesService', propertiesService);

function propertiesService($resource) {
    return $resource('scripts/data/sports.json', {}, {
        'getSports': {
            method:'GET',
            url:'scripts/data/sports.json',
            isArray: true
        },
        'getTeams': {
            method:'GET',
            url:'scripts/data/teams.json',
            isArray: true
        }
    });
}
})();

    