(function(){
'use strict';
/**
 * @ngdoc function
 * @name companyApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the companyApp
 */
angular.module('companyApp')
    .controller('NavbarController', NavbarController);

function NavbarController(authFact,$cookies,$state) {
    var vm = this;
    vm.signOut = function () {
	    //clear authentication
        authFact.setAccessToken(false);
        $cookies.remove('authToken');
        $cookies.remove('userObject');

        //redirect to login page
        $state.go('login');
    }
}

})();


