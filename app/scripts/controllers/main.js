'use strict';

/**
 * @ngdoc function
 * @name companyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the companyApp
 */
angular.module('companyApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
