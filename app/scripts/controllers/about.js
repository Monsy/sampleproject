'use strict';

/**
 * @ngdoc function
 * @name companyApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the companyApp
 */
angular.module('companyApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
