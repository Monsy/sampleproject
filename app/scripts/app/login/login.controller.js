(function(){
'use strict';
/**
 * @ngdoc function
 * @name companyApp.controller:LoginController
 * @description
 * # LoginController
 * Controller of the companyApp
 */
angular.module('companyApp')
    .controller('LoginController', LoginController);

function LoginController(LoginService, $state, authFact, $scope) {
    var vm =  this;
    //variables
    vm.errorMessage = '';
    vm.invalidUser = false; //flag for error message
    vm.user = {
        username: '',
        password: ''
    };
    //functions
    vm.validateCredentials = validateCredentials;
    
    /*
    * @name: validateCredentials
    * @description: this function calls a backend request to validate user credentials
    */
    function validateCredentials() {
        if (vm.user.username != '' && vm.user.password != '') {
            LoginService.query(
                vm.user,
                onFetchSuccess, onFetchError);    
        } else {
            vm.errorMessage = 'Please input your complete credentials';
            showErrorMessage();
        }
    }

    function onFetchSuccess(data, headers) {
        var userList = data;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].username == vm.user.username && userList[i].password == vm.user.password) {

                authFact.setAccessToken(true); //set cookies for logged in user
                authFact.setUserObject(userList[i]); 
                
                $state.go('home', { user: vm.user });
                break;
            } else {
                vm.errorMessage = 'Invalid User';
                showErrorMessage();
            }
        };        
    }

    function onFetchError(error) {
        vm.errorMessage = 'Error in connecting to database';
        showErrorMessage();
    }

    function showErrorMessage() {
        vm.invalidUser = true;
        setTimeout(function() {
            vm.invalidUser = false;
            $scope.$digest();
        }, 1000);
    }
}
})();

