(function(){
'use strict';
/**
 * @ngdoc overview
 * @name companyApp
 * @description
 * # companyApp
 *
 * Main module of the application.
 */
angular.module('companyApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('login', {
            url:'/login',
            views:{
                'content@':{
                    templateUrl:'scripts/app/login/login.html',
                    controller:'LoginController',
                    controllerAs:'vm'
                }
            }
        });
    });  
})();

