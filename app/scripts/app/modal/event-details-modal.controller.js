(function(){
'use strict';
/**
 * @ngdoc function
 * @name companyApp.controller:EventModalController
 * @description
 * # EventModalController
 * Controller of the companyApp
 */
angular.module('companyApp')
    .controller('EventModalController', EventModalController);

function EventModalController($uibModalInstance, $stateParams, EventsService, $rootScope, toaster, sports, teams) {

    var vm = this;
    //variables
    vm.eventObject = {};
    vm.sports = sports;
    vm.teams = teams;
    //for calendar directive
    vm.dateOptions = {};
    vm.dateFormat = 'dd-MMMM-yyyy';
    vm.opened1 = false;
    vm.opened2 = false;
    //functions
    vm.activate = activate;
    vm.submit = submit;

    vm.activate();

    /*
    * @name activate
    * @description on load function
    */
    function activate() {
        vm.eventObject = $stateParams.event; //event object from parent page
        if (vm.eventObject.id != undefined) {
            vm.eventObject.kickOff = new Date(vm.eventObject.kickOff);  //reformat the eventObject
            vm.eventObject.endDate = new Date(vm.eventObject.endDate);
        }
    }

    /*
    * @name submit
    * @description triggers the POST request
    */
    function submit() {
        if (vm.eventObject.id != undefined) {
            // calls an UPDATE method on the event service
            // EventsService.updateEvent(
            //                   {event : vm.eventObject},
            //                   onSuccessSave, onErrorSave
            //               );
        } else {
            // calls an CREATE method on the event service
            // EventsService.createEvent(
            //                   {event : vm.eventObject},
            //                   onSuccessSave, onErrorSave
            //               );
      }
      dummyUpdateListViewModel(vm.eventObject); //for presentation purposes update the list view model
      $uibModalInstance.close('close');
    }

    function onSuccessSave(data, headers){
        // trigger a listener to update 
        // the result set of the events
        $rootScope.$emit('updatelist', {});
    }

    function onErrorSave(error){
        toaster.pop('error', "Error", "Error on updating events");
    }

    /*
    * @name dummyUpdateListViewModel
    * @description update the list of events
    */
    function dummyUpdateListViewModel(eventObject){
        $rootScope.$emit('updatelist', eventObject);
    }

    vm.openCalendar = function (calendar) {
        if(calendar == 'kickoff'){
            vm.opened1 = true;
        } else {
            vm.opened2 = true;
        }
    }

    vm.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

})();
