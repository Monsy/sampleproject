(function(){
'use strict';
/**
 * @ngdoc overview
 * @name companyApp
 * @description
 * # companyApp
 *
 * Main module of the application.
 */
angular.module('companyApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('event-list', {
          url: '/event-list',
          parent: 'entity',
          views: {
              'content@':{
                  templateUrl:'scripts/app/entities/events/event-list.html',
                  controller:'EventListController',
                  controllerAs:'vm'
          }
      }})
      .state('event-modal',{
          parent: 'event-list',
          url: "/event-details",
          params: {
              event:{}
          },
          onEnter: ['$stateParams', '$state', '$uibModal', 
              function ($stateParams, $state, $uibModal) {
                  $uibModal.open({
                      parent:'event-list',
                      templateUrl:'scripts/app/modal/event-details-modal.html',
                      controller:'EventModalController',
                      controllerAs:'vm',
                      backdrop:'static',
                      size:'md',
                      resolve: {
                          sports: function (propertiesService) {
                              return propertiesService.getSports();
                          },
                          teams: function (propertiesService) {
                              return propertiesService.getTeams();
                          },
                      }
                  })
                  .result
                  .then(
                  function() {
                      $state.go('event-list', null, { reload : false });
                  }, 
                  function() {
                      $state.go('event-list');
                  });
              }
          ]
      });
  });
})();

