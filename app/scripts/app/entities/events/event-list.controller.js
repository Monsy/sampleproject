(function(){
'use strict';
/**
 * @ngdoc function
 * @name companyApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the companyApp
 */
angular.module('companyApp')
    .controller('EventListController', EventListController);

function EventListController(EventsService, $rootScope, toaster) {
    var vm = this;
    //variables
    vm.events = []; //list of events
    vm.searchBy = 'sport';
    //functions
    vm.activate = activate;
    vm.deleteEmployee = deleteEmployee;
    vm.search = search;

    vm.activate();

    //event listeners
    $rootScope.$on('updatelist', function (event, eventObject){
        toaster.pop('success', "Saved", "Event List Updated");

        //usually the records must be reloaded from database
        //vm.activate(); 
        
        dummyUpdateProcess(eventObject); //update the result list
    
    });

    /*
    * @name activate
    * @description on load function
    */
    function activate() {
        EventsService.query(onFetchSuccess, onFetchError);
    }

    function onFetchSuccess(data, headers) {
        vm.events = data;
    }

    function onFetchError(error) {
        toaster.pop('error', "Database Error", "No Records Found");
    }

    function deleteEmployee(eventId) {
        // usually calls a POST request to backend
        // EventsService.deleteEmployee(
        //                  {eventId: eventId},
        //                  onSuccessDelete, onErrorDelete
        //               );
        dummyDeleteProcess(eventId);
    }

    function onSuccessDelete(data, headers, eventId) {
        toaster.pop('success', eventId + " deleted", "Event List Updated");

        //reload the result set from the backend
        //vm.activate(); 
    }

    function onErrorDelete(error) {
        toaster.pop('error', "Error", "Failed to delete");
    }

    /*
    * @name search
    * @description returns a record that matched the search values
    */
    function search(event) { 
        var query = angular.lowercase(vm.searchInput);
        if (vm.searchBy == 'sport') {
            return (angular.lowercase(event.sport.description).indexOf(query || '') !== -1);
        } else if (vm.searchBy == 'hometeam') {
            return (angular.lowercase(event.homeTeam.description).indexOf(query || '') !== -1);
        } else if (vm.searchBy == 'awayteam') {
            return (angular.lowercase(event.awayTeam.description).indexOf(query || '') !== -1);   
        }
    }
    
    /*
    * @name dummyDeleteProcess , dummyUpdateProcess
    * @description update just the view model
    */
    function dummyDeleteProcess(eventId) {
        var numberOfEvents = vm.events.length;

        for (var i = 0; i < numberOfEvents; i++) {
            if (vm.events[i].id === eventId) {
                vm.events.splice(i, 1);
                onSuccessDelete({}, {}, eventId);
                break;
            }
        };
    }

    function dummyUpdateProcess(eventObject){
        if (eventObject.id === undefined) { //generate dummy eventId
            var newId = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            for( var i=0; i < 5; i++ ) {
                newId += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            eventObject.id = newId;
            vm.events.push(eventObject);
        } else {
            var numberOfEvents = vm.events.length;
            for (var i = 0; i < numberOfEvents; i++) {
                if (vm.events[i].id == eventObject.id) {
                    vm.events[i] = Object.assign({}, eventObject);
                    break;
                }
            };
        }
    }
}

})();

