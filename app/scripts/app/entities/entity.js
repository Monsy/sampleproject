(function(){
'use strict';
/**
 * @ngdoc overview
 * @name companyApp
 * @description
 * # companyApp
 * Main module of the application.
 */
angular.module('companyApp')
    .config(function ($stateProvider) {
	  $stateProvider
	    .state('entity', {
	          parent: 'site',
	          abstract: true
	    });
    });
})();

