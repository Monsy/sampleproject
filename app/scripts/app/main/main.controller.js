(function(){
'use strict';
/**
 * @ngdoc function
 * @name companyApp.controller:MainController
 * @description
 * # MainController
 * Controller of the companyApp
 */
angular.module('companyApp')
    .controller('MainController', MainController);

function MainController($scope , $stateParams, authFact) {
    var vm = this;
    //variables
    vm.user = {};
    //functions
    vm.activate = activate;

    vm.activate();

    /*
    * @name activate
    * @description on load function
    */
    function activate() {
        vm.user = authFact.getUserObject();
        
        setTimeout(function(){
	    	vm.show = true;
	    	$scope.$digest();
	    }, 100);
    }
}

})();

