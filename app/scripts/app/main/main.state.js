(function(){
'use strict';
/**
 * @ngdoc overview
 * @name companyApp
 * @description
 * # companyApp
 *
 * Main module of the application.
 */
angular.module('companyApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('home', {
            parent: 'site',
            url:'/',
            params:{
                user:''
            },
            views:{
                'content@':{
                    templateUrl:'scripts/app/main/main.html',
                    controller:'MainController',
                    controllerAs: 'vm'
                }
            }
        });
      });  

})();

